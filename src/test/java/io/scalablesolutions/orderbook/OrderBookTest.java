package io.scalablesolutions.orderbook;

import io.scalablesolutions.orders.AskOrder;
import io.scalablesolutions.orders.BidOrder;
import io.scalablesolutions.orders.Order;
import io.scalablesolutions.orders.OrderType;
import io.scalablesolutions.utils.DataGenerators;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static io.scalablesolutions.orderbook.OrderBook.*;
import static org.junit.jupiter.api.Assertions.*;

class OrderBookTest {

    @Test
    @DisplayName("putOrderTest")
    void putOrderTest() {
        OrderBook orderBook = new OrderBook();

        for (int i = 0; i < 2; i++) {
            orderBook.putOrder(new AskOrder(
                    DataGenerators.generateRandomPrice(ASK_MIN_PRICE,ASK_MAX_PRICE),
                    DataGenerators.generateRandomQuantity(MIN_QTY,MAX_QTY)));
        }
        for (int i = 0; i < 2; i++) {
            orderBook.putOrder(new BidOrder(
                    DataGenerators.generateRandomPrice(BID_MIN_PRICE,BID_MAX_PRICE),
                    DataGenerators.generateRandomQuantity(MIN_QTY,MAX_QTY)));
        }
        orderBook.putOrder("test123", new BidOrder(
                DataGenerators.generateRandomPrice(BID_MIN_PRICE,BID_MAX_PRICE),
                DataGenerators.generateRandomQuantity(MIN_QTY,MAX_QTY)));

        assertEquals(orderBook.getSortedOrders().size(), 5);
    }

    @Test
    @DisplayName("removeOrderTest")
    void removeOrderTest() {
        OrderBook orderBook = new OrderBook();

        orderBook.putOrder("AskOrder", new AskOrder(1.1,10));
        orderBook.putOrder("BidOrder", new BidOrder(1.1,10));

        orderBook.removeOrder("AskOrder");
        assertEquals(orderBook.getSortedOrders().size(), 1);
        assertEquals(orderBook.getSortedOrders().get(0).getOrderId(), "BidOrder");
    }

    @Test
    @DisplayName("getOrderInfoTest")
    void getOrderInfoTest() {
        OrderBook orderBook = new OrderBook();

        orderBook.putOrder(new AskOrder(
                DataGenerators.generateRandomPrice(ASK_MIN_PRICE,ASK_MAX_PRICE),
                DataGenerators.generateRandomQuantity(MIN_QTY,MAX_QTY)));
        orderBook.putOrder("BidOrder", new BidOrder(1.1,10));

        Order orderInfo = orderBook.getOrderInfo("BidOrder");

        assertEquals(orderInfo.getOrderId(), "BidOrder");
        assertEquals(orderInfo.getOrderType(), OrderType.BID);
        assertEquals(orderInfo.getPrice(), 1.1);
        assertEquals(orderInfo.getQuantity(), 10);
    }

    @Test
    @DisplayName("getSnapshotTest")
    void getSnapshotTest() {
        OrderBook orderBook = new OrderBook();

        for (int i = 0; i < 3; i++) {
            orderBook.putOrder(new AskOrder(
                    DataGenerators.generateRandomPrice(ASK_MIN_PRICE,ASK_MAX_PRICE),
                    DataGenerators.generateRandomQuantity(MIN_QTY,MAX_QTY)));
        }
        for (int i = 0; i < 5; i++) {
            orderBook.putOrder(new BidOrder(
                    DataGenerators.generateRandomPrice(BID_MIN_PRICE,BID_MAX_PRICE),
                    DataGenerators.generateRandomQuantity(MIN_QTY,MAX_QTY)));
        }

        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(OrderBookTest.class.getResourceAsStream("/OrderBook_Schema.json")));
        JSONObject jsonSubject = new JSONObject(orderBook.getOrdersSnapshot().toString());

        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(jsonSubject);
    }

    @Test
    @DisplayName("getOrdersSnapshotAndSaveTest")
    void getSnapshotAndSaveTest() {
        OrderBook orderBook = new OrderBook();

        for (int i = 0; i < 3; i++) {
            orderBook.putOrder(new AskOrder(
                    DataGenerators.generateRandomPrice(ASK_MIN_PRICE,ASK_MAX_PRICE),
                    DataGenerators.generateRandomQuantity(MIN_QTY,MAX_QTY)));
        }
        for (int i = 0; i < 5; i++) {
            orderBook.putOrder(new BidOrder(
                    DataGenerators.generateRandomPrice(BID_MIN_PRICE,BID_MAX_PRICE),
                    DataGenerators.generateRandomQuantity(MIN_QTY,MAX_QTY)));
        }

        orderBook.getOrdersSnapshotAndSave("src/test/resources/Orders_Snapshot.json");
        String savedJson = "";
        try {
            savedJson = Files.readString(Path.of("src/test/resources/Orders_Snapshot.json"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(OrderBookTest.class.getResourceAsStream("/OrderBook_Schema.json")));
        JSONObject jsonSubject = new JSONObject(savedJson);

        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(jsonSubject);
    }
}