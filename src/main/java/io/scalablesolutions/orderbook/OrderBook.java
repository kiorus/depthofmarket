package io.scalablesolutions.orderbook;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.scalablesolutions.orders.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

/**
 * Класс OrderBook, реализующий упрощенный биржевой стакан.
 *
 * @author  Sergey Pestsov
 * @version 1.0
 * @since   04.02.2021
 */
public class OrderBook {

    public static final double BID_MIN_PRICE = 10.0;
    public static final double BID_MAX_PRICE = 50.0;
    public static final double ASK_MIN_PRICE = 50.01;
    public static final double ASK_MAX_PRICE = 100.0;
    public static final long MIN_QTY = 1;
    public static final long MAX_QTY = 100;

    private final Map<String, Order> orderBook = new HashMap<>();

    /**
     * Сортировка биржевого стакана.
     *
     * @return возвращает список всех заявок в отсортированном виде, от большей цены к меньшей.
     */
    public ArrayList<Order> getSortedOrders() {
        ArrayList<Order> orders = new ArrayList<>(orderBook.values());
        orders.sort(Comparator.comparing(Order::getPrice).reversed());

        return orders;
    }

    /**
     * Постановка заявок в биржевой стакан. С указанием произвольного номера заявки (для тестов).
     *
     * @param orderId номер заявки
     * @param order заявка
     * @param <T> тип заявки, например {@link AskOrder} или {@link BidOrder}
     * @see AskOrder
     * @see BidOrder
     */
    public <T extends Order> void putOrder(String orderId, T order) {
        order.setOrderId(orderId);
        orderBook.put(orderId, order);
    }

    /**
     * Постановка заявок в бирженовй стакан. С автоматической генерацией номера заявки .
     *
     * @param order заявка
     * @param <T> тип заявки, например {@link AskOrder} или {@link BidOrder}
     * @see OrderId
     * @see AskOrder
     * @see BidOrder
     */
    public <T extends Order> void putOrder(T order) {
        String orderId = new OrderId().getOrderId();
        order.setOrderId(orderId);
        orderBook.put(orderId, order);
    }

    /**
     * Снятие заявок по идентификатору заявки
     *
     * @param orderId номер заявки
     */
    public void removeOrder(String orderId) {
        orderBook.remove(orderId);
    }

    /**
     * Получение данных заявки по идентификатору
     *
     * @param orderId номер заявки
     * @return возвращает информацию о заявке
     */
    public Order getOrderInfo(String orderId) {
        return orderBook.get(orderId);
    }

    /**
     * Получение снапшота рыночных данных
     *
     * @return возвращает данные о биржевом стакане в формате JSON
     */
    public ObjectNode getOrdersSnapshot() {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode json = mapper.createObjectNode();
        ArrayNode askArray = mapper.createArrayNode();
        ArrayNode bidArray = mapper.createArrayNode();

        for (Order order : getSortedOrders()) {
            if (order.getOrderType().equals(OrderType.ASK)) {
                askArray.addObject()
                        .put("orderId", order.getOrderId())
                        .put("price", order.getPrice())
                        .put("quantity", order.getQuantity());
            }
            else {
                bidArray.addObject()
                        .put("orderId", order.getOrderId())
                        .put("price", order.getPrice())
                        .put("quantity", order.getQuantity());
            }

        }
        json.set("ask", askArray);
        json.set("bid", bidArray);

        return json;
    }

    /**
     * Получение снапшота рыночных данных с сохранением в файл
     *
     * @param path путь до файла
     * @return возвращает данные о биржевом стакане в формате JSON
     * @see OrderBook#getOrdersSnapshot()
     */
    public ObjectNode getOrdersSnapshotAndSave(String path) {
        ObjectNode json = getOrdersSnapshot();
        Path p = Paths.get("",path);

        try {
            if (Files.notExists(p)) {
                Files.createFile(p);
            }

            Files.writeString(p, json.toPrettyString(), StandardOpenOption.WRITE);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }
}
