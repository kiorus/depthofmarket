package io.scalablesolutions.orders;

/**
 * Перечисление OrderType - содержит в себе возможные типы заявок
 *
 * @author  Sergey Pestsov
 * @version 1.0
 * @since   04.02.2021
 */
public enum OrderType {

    ASK,
    BID

}
