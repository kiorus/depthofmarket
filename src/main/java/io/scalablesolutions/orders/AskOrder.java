package io.scalablesolutions.orders;

/**
 * Класс AskOrder - заявки на продажу
 *
 * @author  Sergey Pestsov
 * @version 1.0
 * @since   04.02.2021
 */
public class AskOrder extends AbstractOrder {

    /**
     * @param price цена заявки
     * @param quantity количество ценных бумаг в заявке
     */
    public AskOrder(double price, long quantity) {
        setOrderType(OrderType.ASK);
        setPrice(price);
        setQuantity(quantity);
    }

}
