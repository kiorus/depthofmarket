package io.scalablesolutions.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Абстрактный класс AbstractOrder - описывает общее состояние и поведение, которым будут обладать его наследники.
 *
 * @author  Sergey Pestsov
 * @version 1.0
 * @since   04.02.2021
 */
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractOrder implements Order {

    /**
     * цена заявки
     */
    private double price;

    /**
     * количество ценных бумаг в заявке
     */
    private long quantity;

    /**
     * тип заявки
     *
     * @see OrderType
     */
    private OrderType orderType;

    /**
     * идентификатор заявки
     *
     * @see OrderId
     */
    private String orderId;

}
