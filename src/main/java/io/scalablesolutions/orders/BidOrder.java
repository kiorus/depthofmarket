package io.scalablesolutions.orders;

/**
 * Класс AskOrder - заявки на покупку
 *
 * @author  Sergey Pestsov
 * @version 1.0
 * @since   04.02.2021
 */
public class BidOrder extends AbstractOrder {

    /**
     * @param price цена заявки
     * @param quantity количество ценных бумаг в заявке
     */
    public BidOrder(double price, long quantity) {
        setOrderType(OrderType.BID);
        setPrice(price);
        setQuantity(quantity);
    }

}
