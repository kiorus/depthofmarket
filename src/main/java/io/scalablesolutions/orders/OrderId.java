package io.scalablesolutions.orders;

import lombok.*;

import static java.util.UUID.randomUUID;

/**
 * Класс OrderId - предназначен для работы с идентификаторами заявок
 *
 * @author  Sergey Pestsov
 * @version 1.0
 * @since   04.02.2021
 */
@Getter
@Setter
@RequiredArgsConstructor
public class OrderId {

    /**
     * Идентификатор заявки
     */
    @NonNull
    private String orderId;

    /**
     * В конструкторе без параметров автоматически генерируется UUID-идентификатор и присваивается заявке
     * Пример UUID-идентификатора: {@code "43b1e66f-228c-44e6-8ab6-a0da4183b44d"}
     */
    public OrderId() {
        this.orderId =  randomUUID().toString();
    }

}
