package io.scalablesolutions.orders;

/**
 * Интерфейс Order - описывает поведение, которое должно быть присуще всем видам заявок
 *
 * @author  Sergey Pestsov
 * @version 1.0
 * @since   04.02.2021
 */
public interface Order {

    /**
     * Установить цену завке
     *
     * @param price цена
     */
    void setPrice(double price);

    /**
     * @return возвращает цену заявки
     */
    double getPrice();

    /**
     * Установить количество ценных бумаг в заявке
     *
     * @param quantity количество ценных бумаг
     */
    void setQuantity(long quantity);

    /**
     * @return возвращает количество ценных бумаг в заявке
     */
    long getQuantity();

    /**
     * Установить тип завяки
     *
     * @param orderType тип заявки
     * @see OrderType
     */
    void setOrderType(OrderType orderType);

    /**
     * @return возвращает тип заявки
     * @see OrderType
     */
    OrderType getOrderType();

    /**
     * Установить номер заявки
     *
     * @param orderId номер заявки
     * @see OrderId
     */
    void setOrderId(String orderId);

    /**
     * @return получить номер заявки
     */
    String getOrderId();

}
