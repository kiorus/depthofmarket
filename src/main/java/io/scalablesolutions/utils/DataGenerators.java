package io.scalablesolutions.utils;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Класс DataGenerators - вспомогательный класс для генерации различных данных
 *
 * @author  Sergey Pestsov
 * @version 1.0
 * @since   04.02.2021
 */
public class DataGenerators {

    /**
     * Генерация цены за заявку
     *
     * @param min минимальное значение
     * @param max максимальное значение
     * @return возвращает случайное значение цены в интервале между min и max
     */
    public static double generateRandomPrice(double min, double max) {
        double rnd = ThreadLocalRandom.current().nextDouble(min, max);
        return Math.round(rnd * 100.0) / 100.0;
    }

    /**
     * Генерация количества ценных бумаг в заявке
     *
     * @param min минимальное значение
     * @param max максимальное значение
     * @return возвращает случайное значение количества в интервале между min и max
     */
    public static long generateRandomQuantity(long min, long max) {
        return ThreadLocalRandom.current().nextLong(min, max);
    }

}
